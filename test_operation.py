import unittest
from operation import summation


class TestOperation(unittest.TestCase):

    def test_summation(self):
        self.assertEqual(summation(5, 10), 15)

        self.assertEqual(summation(5.2, 5.5), 10.7)

        self.assertEqual(summation(2, 5.5), 7.5)

        self.assertNotEqual(summation(), 0)

        self.assertNotEqual(summation(), 0)
        self.assertNotEqual(summation(), 0)


if __name__ == '__main__':
    unittest.main()
