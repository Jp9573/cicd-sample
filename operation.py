from random import randint


def summation(a=None, b=None):
    if a is None and b is None:
        a = randint(10, 10000)
        b = randint(1000, 99999)

    return a + b
